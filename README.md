Multiple version combinations are avaiable as tags. The tags are in the format `<kubectl-version>-<gcloud-version>-alpine<alpine-version>`. For example, `1.27.8-473.0.0-alpine3.19`.

The default ENTRYPOINT of this image is the default entrypoint of the `python:3.12-alpine${ALPINE_VERSION}` image. Use `kubectl` as the argument to run the `kubectl` command.

If you are missing a version, please open an issue.